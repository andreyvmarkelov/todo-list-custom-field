(function($) {
    $(document).ready(function() {
        initTodoList();

        JIRA.bind(JIRA.Events.NEW_CONTENT_ADDED, function (e, context, reason) {
            if (reason !== JIRA.CONTENT_ADDED_REASON.panelRefreshed) {}
        });

        $('tbody.todolist').sortable();
    });

    function initTodoList() {
        $("span.tododelete").live("click", function(event) {
            removeToDoListItem($(this));
            $('tbody.todolist').sortable();
        });

        $("span.todoedit").live("click", function(event) {
            editToDoListItem($(this));
            $('tbody.todolist').sortable();
        });

        $("span.todoadd").live("click", function(event) {
            addToDoListItem($(this));
            $('tbody.todolist').sortable();
        });
    }

    function addToDoListItem(source) {
        var currentFieldId = $(source).attr("data-cfid");
        var currentInput = $(source).closest("tr").find(".todoinput");
        var currentValue = $(currentInput).val();
        if (currentValue) {
            var sharesObj = $.evalJSON($("#" + currentFieldId).val());
            for (var objId in sharesObj) {
                if (sharesObj[objId]["id"] == currentValue) {
                    $(currentInput).animate({backgroundColor: "red"}, 500, function() {
                        $(currentInput).animate({backgroundColor: "white"}, 500);
                    });
                    return;
                }
            }

            var rowCount = $("#todo_" + currentFieldId).attr("data-table-count");
            var newRow = $("<tr>").css({"height": "16px"}).attr("data-row-id", ++rowCount);
            var td1 = $("<td>").addClass("checkboxcolumn").css({"width": "16px"})
                .append($("<input>", {type: "checkbox", value: "true"}).addClass("todocheckbox").change(function(event) { ru_mail_changeToDoItem(event, currentFieldId)}));
            var td2 = $("<td>").addClass("wrapcolumn")
                .append($("<span>").addClass("todoitem todo_undone").html(currentValue));
            var td3 = $("<td>").css({"width": "16px"})
                .append($("<span>").addClass("aui-button aui-button-subtle todoedit").attr("data-cfid", currentFieldId)
                .append($("<span>").addClass("aui-icon aui-icon-small aui-iconfont-edit").text("Edit")));
            var td4 = $("<td>").css({"width": "16px"})
                .append($("<span>").addClass("aui-button aui-button-subtle tododelete").attr("data-cfid", currentFieldId)
                .append($("<span>").addClass("aui-icon aui-icon-small aui-iconfont-delete").text("Delete")));
            $("#todo_" + currentFieldId).append(newRow.append(td1).append(td2).append(td3).append(td4));

            $(currentInput).val("");
            var itemObj = new Object();
            itemObj["id"] = currentValue;
            itemObj["type"] = "todo";
            sharesObj.push(itemObj);
            $("#" + currentFieldId).val($.toJSON(sharesObj));
        }
    }

    function removeToDoListItem(source) {
        var currentFieldId = $(source).attr("data-cfid");
        var row = $(source).closest("tr");
        var currentValue = $(row).find(".todoitem").text();

        var sharesObj = $.evalJSON($("#" + currentFieldId).val());
        var num = -1;
        for (var objId in sharesObj) {
            if (sharesObj[objId]["id"] == currentValue) {
                num = objId;
                break;
            }
        }
        if (num != -1) {
            sharesObj.splice(num, 1);
        }

        $("#" + currentFieldId).val($.toJSON(sharesObj));
        row.remove();
    }

    function editToDoListItem(source) {
        var currentFieldId = $(source).attr("data-cfid");
        var row = $(source).closest("tr");
        var rowId = $(row).attr("data-row-id");

        var areaId = currentFieldId + "_" + rowId;
        // do nothing if area already exists
        if ($("#todoeditbox_" + areaId).length) {
            return;
        }

        var currentValue = $(row).find(".todoitem").text();
        var textarea = $("<textarea class='textarea todolist-area' id='todoeditarea_" + areaId + "'></textarea>").val(currentValue);
        var saveBtn = $("<span class='aui-button'></span>")
                .text(AJS.I18n.getText("ru.andreymarkelov.atlas.plugins.todos.savetodo"))
                .click(function() {
                    var currentTextarea = $("#todoeditarea_" + areaId);
                    if (!currentTextarea.val()) return;

                    var sharesObj = $.evalJSON($("#" + currentFieldId).val());
                    for (var objId in sharesObj) {
                        if (sharesObj[objId]["id"] == currentValue) {
                            sharesObj[objId]["id"] = currentTextarea.val();
                            $("#" + currentFieldId).val($.toJSON(sharesObj));
                        }
                    }
                    $("#todoeditbox_" + areaId).remove();
                    $(row).find(".todoitem").html(currentTextarea.val()).show();
                });
        var cancelBtn = $("<span class='aui-button aui-button-link todolist-cancel'></span>")
                .text(AJS.I18n.getText("ru.andreymarkelov.atlas.plugins.todos.canceltodo"))
                .click(function() {
                    $("#todoeditbox_" + areaId).remove();
                    $(row).find(".todoitem").show();
                });
        var btnBox = $("<div class='todolist-edit-btns'></div>").append(saveBtn).append(cancelBtn);
        var editBox = $("<div id='todoeditbox_" + areaId + "'></div>").append(textarea).append(btnBox);
        $(row).find(".todoitem").hide().after(editBox);
    }
})(AJS.$);

function ru_mail_changeToDoItem(event, cfId) {
    var target = jQuery(event.target);
    var sharesObj = jQuery.evalJSON(jQuery("#" + cfId).val());
    var targetToDo = jQuery(target).parent().next().find("span").text();
    for (var objId in sharesObj) {
        if (sharesObj[objId]["id"] == targetToDo) {
            if (jQuery(target).attr('checked')) {
                sharesObj[objId]["type"] = 'done';
                jQuery(target).parent().next().children().removeClass("todo_undone").addClass("todo_done");
            } else {
                sharesObj[objId]["type"] = 'todo';
                jQuery(target).parent().next().children().removeClass("todo_done").addClass("todo_undone");
            }
            jQuery("#" + cfId).val(jQuery.toJSON(sharesObj));
        }
    }
}
