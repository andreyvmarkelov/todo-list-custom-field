<atlassian-plugin key="${project.groupId}.${project.artifactId}" name="${project.name}" plugins-version="2">
    <plugin-info>
        <description>${project.description}</description>
        <version>${project.version}</version>
        <vendor name="${project.organization.name}" url="${project.organization.url}" />
        <param name="plugin-icon">/ru/andreymarkelov/atlas/plugins/todos/images/todoIcon2.png</param>
        <param name="plugin-logo">/ru/andreymarkelov/atlas/plugins/todos/images/todoIcon1.png</param>
        <param name="vendor-icon">/ru/andreymarkelov/atlas/plugins/todos/images/32.jpg</param>
        <param name="vendor-logo">/ru/andreymarkelov/atlas/plugins/todos/images/144.jpg</param>
        <param name="configure.url">/secure/ToDoListConfigAction!default.jspa</param>
        <param name="atlassian-data-center-compatible">true</param>
    </plugin-info>

    <!-- I18N -->
    <resource type="i18n" name="i18n" location="ru.andreymarkelov.atlas.plugins.todos.i18n.todolist"/>

    <!-- Custom Fields -->
    <customfield-type key="todo-list-custom-field" name="ToDo List custom field" class="ru.andreymarkelov.atlas.plugins.todos.field.ToDoListCf">
        <description>This is a simple checklist custom field.</description>
        <resource type="velocity" name="view" location="/ru/andreymarkelov/atlas/plugins/todos/template/field/todolist-cf-view.vm"/>
        <resource type="velocity" name="column-view" location="/ru/andreymarkelov/atlas/plugins/todos/template/field/todolist-cf-column-view.vm"/>
        <resource type="velocity" name="xml" location="/ru/andreymarkelov/atlas/plugins/todos/template/field/todolist-cf-xml.vm"/>
        <resource type="velocity" name="edit" location="/ru/andreymarkelov/atlas/plugins/todos/template/field/todolist-cf-edit.vm"/>
        <resource type="download" name="customfieldpreview.png" location="/ru/andreymarkelov/atlas/plugins/todos/images/todocf.png"/>
    </customfield-type>

    <!-- Field Searchers -->
    <customfield-searcher key="todo-list-custom-field-searcher" name="ToDo List Custom Field Searcher" class="ru.andreymarkelov.atlas.plugins.todos.field.ToDoFieldSearcher">
        <description>This is the ToDo list custom field searcher</description>
        <resource type="velocity" name="label" location="/ru/andreymarkelov/atlas/plugins/todos/template/field/todolist-cf-label.vm"/>
        <resource type="velocity" name="search" location="/ru/andreymarkelov/atlas/plugins/todos/template/field/todolist-cf-search.vm"/>
        <resource type="velocity" name="view" location="/ru/andreymarkelov/atlas/plugins/todos/template/field/todolist-cf-view-search.vm"/>
        <valid-customfield-type package="${project.groupId}.${project.artifactId}" key="todo-list-custom-field"/>
    </customfield-searcher>

    <!-- Web Resources -->
    <web-resource key="todolist-resources" name="todolist Web Resources">
        <transformation extension="js">
            <transformer key="jsI18n"/>
        </transformation>
        <dependency>com.atlassian.auiplugin:aui-experimental-tooltips</dependency>
        <dependency>com.atlassian.auiplugin:aui-experimental-restfultable</dependency>
        <resource type="download" name="todolist.css" location="/ru/andreymarkelov/atlas/plugins/todos/css/todolist.css"/>
        <resource type="download" name="todolist.js" location="/ru/andreymarkelov/atlas/plugins/todos/js/todolist.js"/>
        <resource type="download" name="jquery.json-2.3.js" location="/ru/andreymarkelov/atlas/plugins/todos/js/jquery.json-2.3.js"/>
        <resource type="download" name="images/" location="/images"/>
        <context>atl.general</context>
        <context>atl.admin</context>
    </web-resource>

    <!-- Web Items -->
    <web-section key="todolist-admin-section" location="admin_plugins_menu">
        <label key="ru.andreymarkelov.atlas.plugins.todos.admin.section"/>
    </web-section>
    <web-item key="todolist-configuration-link" name="ToDo Lists Configuration" section="admin_plugins_menu/todolist-admin-section" weight="200">
        <label key="ru.andreymarkelov.atlas.plugins.todos.admin.config.name"/>
        <condition class="com.atlassian.jira.plugin.webfragment.conditions.JiraGlobalPermissionCondition"><param name="permission">admin</param></condition>
        <link linkId="todolist-configuration-link">/secure/ToDoListConfigAction!default.jspa</link>
    </web-item>

    <!-- Actions -->
    <webwork1 key="todolist_conf" name="ToDo list fields configuration" class="java.lang.Object">
        <actions>
            <action name="ru.andreymarkelov.atlas.plugins.todos.action.ToDoListConfigAction" alias="ToDoListConfigAction">
                <view name="input">/ru/andreymarkelov/atlas/plugins/todos/template/action/todo-list-conf.vm</view>
                <view name="success">/ru/andreymarkelov/atlas/plugins/todos/template/action/todo-list-conf.vm</view>
                <view name="permissionviolation">/secure/views/permissionviolation.jsp</view>
                <view name="configure">/ru/andreymarkelov/atlas/plugins/todos/template/action/todo-list-conf-edit.vm</view>
            </action>
        </actions>
    </webwork1>

    <!-- Components -->
    <component key="todolist-data" name="ToDo list plugin data" class="ru.andreymarkelov.atlas.plugins.todos.manager.ToDoDataImpl">
        <interface>ru.andreymarkelov.atlas.plugins.todos.manager.ToDoData</interface>
    </component>
    <component key="todolist-helper" name="ToDo list field helper" class="ru.andreymarkelov.atlas.plugins.todos.field.ToDoListCfHelperImpl">
        <interface>ru.andreymarkelov.atlas.plugins.todos.field.ToDoListCfHelper</interface>
    </component>

    <!-- Component Imports -->
    <component-import key="pluginSettingsFactory">
        <interface>com.atlassian.sal.api.pluginsettings.PluginSettingsFactory</interface>
    </component-import>
    <component-import key="applicationProperties">
        <interface>com.atlassian.sal.api.ApplicationProperties</interface>
    </component-import>

    <!-- Workflow -->
    <workflow-validator key="todolist-all-done-validator" name="ToDo list all done validator" class="ru.andreymarkelov.atlas.plugins.todos.ToDoListAllDoneValidatorFactory">
        <description>This validator checks that all todo items of a ToDo list field is done.</description>
        <validator-class>ru.andreymarkelov.atlas.plugins.todos.ToDoListAllDoneValidator</validator-class>
        <resource type="velocity" name="view" location="/ru/andreymarkelov/atlas/plugins/todos/template/validator/todo-list-view-alldone-validator.vm"/>
        <resource type="velocity" name="input-parameters" location="/ru/andreymarkelov/atlas/plugins/todos/template/validator/todo-list-edit-alldone-validator.vm"/>
        <resource type="velocity" name="edit-parameters" location="/ru/andreymarkelov/atlas/plugins/todos/template/validator/todo-list-edit-alldone-validator.vm"/>
    </workflow-validator>

    <workflow-condition key="todolist-all-done-condition" name="ToDo list all done condition" class="ru.andreymarkelov.atlas.plugins.todos.ToDoListAllDoneConditionFactory">
        <description>This condition checks that all todo items of a ToDo list field is done.</description>
        <condition-class>ru.andreymarkelov.atlas.plugins.todos.ToDoListAllDoneCondition</condition-class>
        <resource type="velocity" name="view" location="/ru/andreymarkelov/atlas/plugins/todos/template/condition/todo-list-view-alldone-condition.vm"/>
        <resource type="velocity" name="input-parameters" location="/ru/andreymarkelov/atlas/plugins/todos/template/condition/todo-list-edit-alldone-condition.vm"/>
        <resource type="velocity" name="edit-parameters" location="/ru/andreymarkelov/atlas/plugins/todos/template/condition/todo-list-edit-alldone-condition.vm"/>
    </workflow-condition>

    <!-- Tabs -->
    <issue-tabpanel key="todolist-all-issue-tab" name="ToDo Information" class="ru.andreymarkelov.atlas.plugins.todos.tab.ToDoIssueTab">
        <resource type="i18n" name="i18n" location="todolist"/>
        <description>This tab shows issue information about all changes of issue ToDo List fields.</description>
        <label key="ru.andreymarkelov.atlas.plugins.todos.issuetab"/>
        <resource type="velocity" name="view" location="/ru/andreymarkelov/atlas/plugins/todos/template/tab/todo-list-issue-tab.vm"/>
        <supports-ajax-load>true</supports-ajax-load>
        <sortable>false</sortable>
    </issue-tabpanel>
</atlassian-plugin>
