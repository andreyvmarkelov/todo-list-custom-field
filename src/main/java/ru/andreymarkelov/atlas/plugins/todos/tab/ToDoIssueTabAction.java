package ru.andreymarkelov.atlas.plugins.todos.tab;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.changehistory.ChangeHistoryManager;
import com.atlassian.jira.plugin.issuetabpanel.AbstractIssueAction;
import com.atlassian.jira.plugin.issuetabpanel.IssueTabPanelModuleDescriptor;

import ru.andreymarkelov.atlas.plugins.todos.ToDoHistoryCollector;
import ru.andreymarkelov.atlas.plugins.todos.manager.ToDoData;
import ru.andreymarkelov.atlas.plugins.todos.model.HistoryToDoFieldData;
import ru.andreymarkelov.atlas.plugins.todos.util.ToDoRenderer;

public class ToDoIssueTabAction extends AbstractIssueAction {
    private final ToDoData pluginData;
    private final ChangeHistoryManager changeHistoryManager;
    private final CustomFieldManager customFieldManager;
    private final Issue issue;

    public ToDoIssueTabAction(
            IssueTabPanelModuleDescriptor descriptor,
            ToDoData pluginData,
            Issue issue,
            ChangeHistoryManager changeHistoryManager,
            CustomFieldManager customFieldManager) {
        super(descriptor);
        this.pluginData = pluginData;
        this.issue = issue;
        this.changeHistoryManager = changeHistoryManager;
        this.customFieldManager = customFieldManager;
    }

    @Override
    public Date getTimePerformed() {
        return issue.getUpdated();
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    @Override
    protected void populateVelocityParams(Map params) {
        ToDoHistoryCollector historyCollector = new ToDoHistoryCollector(pluginData, changeHistoryManager, customFieldManager);
        List<HistoryToDoFieldData> data = historyCollector.getIssueTodos(issue);
        params.put("data", data);
        params.put("renderer", new ToDoRenderer());
    }
}
