package ru.andreymarkelov.atlas.plugins.todos.field;

import ru.andreymarkelov.atlas.plugins.todos.model.ToDoItem;

import java.util.Set;

public interface ToDoListCfHelper {
    Set<ToDoItem> getItems(String value);
}
