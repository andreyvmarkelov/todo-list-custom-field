package ru.andreymarkelov.atlas.plugins.todos.manager;

import ru.andreymarkelov.atlas.plugins.todos.model.ToDoDataItem;

public interface ToDoData {
    ToDoDataItem getToDoDataItem(String cfId);

    void setToDoDataItem(String cfId, ToDoDataItem item);
}
