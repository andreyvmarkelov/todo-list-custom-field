package ru.andreymarkelov.atlas.plugins.todos.util;

import java.util.Date;
import java.util.StringTokenizer;

import org.apache.commons.lang3.StringUtils;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.datetime.DateTimeFormatter;
import com.atlassian.jira.datetime.DateTimeFormatterFactory;
import com.atlassian.jira.plugin.userformat.ProfileLinkUserFormat;
import com.atlassian.jira.plugin.userformat.UserFormats;
import com.atlassian.jira.security.JiraAuthenticationContext;

public class ToDoRenderer {
    private final DateTimeFormatter dateTimeFormatter;
    private final UserFormats userFormats;

    public ToDoRenderer() {
        JiraAuthenticationContext jiraAuthenticationContext = ComponentAccessor.getJiraAuthenticationContext();
        DateTimeFormatterFactory dateTimeFormatterFactory = ComponentAccessor.getComponent(DateTimeFormatterFactory.class);
        this.dateTimeFormatter = (jiraAuthenticationContext.isLoggedInUser()) ? dateTimeFormatterFactory.formatter().forLoggedInUser() : dateTimeFormatterFactory.formatter().withDefaultLocale();
        this.userFormats = ComponentAccessor.getOSGiComponentInstanceOfType(UserFormats.class);
    }

    public String renderDatetime(long date) {
        return dateTimeFormatter.forLoggedInUser().format(new Date(date));
    }

    public String renderExecutor(String username) {
        if (StringUtils.isNotBlank(username)) {
            return userFormats.formatter(ProfileLinkUserFormat.TYPE).formatUsername(username, "user-bean");
        }
        return "";
    }

    public String renderToDo(String todo) {
        StringBuilder sb = new StringBuilder();
        StringTokenizer st = new StringTokenizer(todo, "\n");
        while (st.hasMoreElements()) {
            sb.append(st.nextElement());
            if (st.hasMoreElements()) {
                sb.append("<br/>");
            }
        }
        return sb.toString();
    }
}
