package ru.andreymarkelov.atlas.plugins.todos;

import com.atlassian.jira.util.json.JSONArray;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import org.apache.log4j.Logger;
import ru.andreymarkelov.atlas.plugins.todos.model.ToDoDataItem;

import java.util.ArrayList;
import java.util.List;

public class ToDoDataItemTranslator {
    private static final Logger logger = Logger.getLogger(ToDoDataItemTranslator.class);

    public static ToDoDataItem getToDoDataItemFromString(Object object) {
        if (object == null) {
            return null;
        }

        try {
            JSONObject jsonObj = new JSONObject(object.toString());
            ToDoDataItem data = new ToDoDataItem();
            if (jsonObj.has("isReporter")) {
                data.setReporter(jsonObj.getBoolean("isReporter"));
            }
            if (jsonObj.has("isNobody")) {
                data.setNobody(jsonObj.getBoolean("isNobody"));
            }
            if (jsonObj.has("showPanel")) {
                data.setShowPanel(jsonObj.getBoolean("showPanel"));
            }

            if (jsonObj.has("style")) {
                data.setStyle(jsonObj.getString("style"));
            }
            if (jsonObj.has("groups")) {
                data.setGroups(parseJsonArray(jsonObj.getJSONArray("groups")));
            }
            if (jsonObj.has("statuses")) {
                data.setStatuses(parseJsonArray(jsonObj.getJSONArray("statuses")));
            }
            return data;
        } catch (JSONException e) {
            logger.error("Error parse JSON", e);
            return null;
        }
    }

    private static List<String> parseJsonArray(JSONArray arr) throws JSONException {
        List<String> list = new ArrayList<String>();
        if (arr != null) {
            for (int i = 0; i < arr.length(); i++) {
                list.add(arr.getString(i));
            }
        }
        return list;
    }

    public static String storeToDoDataItemToString(ToDoDataItem obj) {
        if (obj == null) {
            return null;
        }

        JSONObject jsonObj = new JSONObject();
        try {
            jsonObj.put("isReporter", obj.isReporter());
            jsonObj.put("isNobody", obj.isNobody());
            jsonObj.put("showPanel", obj.isShowPanel());
            jsonObj.put("style", obj.getStyle());
            jsonObj.put("groups", obj.getGroups());
            jsonObj.put("statuses", obj.getStatuses());
        } catch (JSONException e) {
            logger.error("Error write JSON", e);
            return null;
        }
        return jsonObj.toString();
    }

    private ToDoDataItemTranslator() {
    }
}
