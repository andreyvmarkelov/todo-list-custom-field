package ru.andreymarkelov.atlas.plugins.todos.tab;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.changehistory.ChangeHistoryManager;
import com.atlassian.jira.plugin.issuetabpanel.IssueAction;
import com.atlassian.jira.plugin.issuetabpanel.IssueTabPanel;
import com.atlassian.jira.plugin.issuetabpanel.IssueTabPanelModuleDescriptor;
import com.atlassian.jira.user.ApplicationUser;

import ru.andreymarkelov.atlas.plugins.todos.ToDoHistoryCollector;
import ru.andreymarkelov.atlas.plugins.todos.manager.ToDoData;
import ru.andreymarkelov.atlas.plugins.todos.model.ToDoDataItem;

public class ToDoIssueTab implements IssueTabPanel {
    private final ToDoData pluginData;
    private final ChangeHistoryManager changeHistoryManager;
    private final CustomFieldManager customFieldManager;
    private IssueTabPanelModuleDescriptor descriptor;

    public ToDoIssueTab(ToDoData pluginData, ChangeHistoryManager changeHistoryManager, CustomFieldManager customFieldManager) {
        this.pluginData = pluginData;
        this.changeHistoryManager = changeHistoryManager;
        this.customFieldManager = customFieldManager;
    }

    @Override
    public List<IssueAction> getActions(Issue issue, ApplicationUser user) {
        List<IssueAction> panelActions = new ArrayList<IssueAction>();
        panelActions.add(new ToDoIssueTabAction(descriptor, pluginData, issue, changeHistoryManager, customFieldManager));
        return panelActions;
    }

    @Override
    public void init(IssueTabPanelModuleDescriptor descriptor) {
        this.descriptor = descriptor;
    }

    @Override
    public boolean showPanel(Issue issue, ApplicationUser user) {
        ToDoHistoryCollector historyCollector = new ToDoHistoryCollector(pluginData, changeHistoryManager, customFieldManager);
        Map<String, String> fields = historyCollector.getToDoFieldNames(issue);
        for (Map.Entry<String, String> entry : fields.entrySet()) {
            ToDoDataItem data = pluginData.getToDoDataItem(entry.getValue());
            if (data != null && data.isShowPanel()) {
                return true;
            }
        }
        return false;
    }
}
