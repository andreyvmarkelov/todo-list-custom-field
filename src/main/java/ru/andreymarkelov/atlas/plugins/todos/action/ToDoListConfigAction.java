package ru.andreymarkelov.atlas.plugins.todos.action;

import java.util.ArrayList;
import java.util.List;

import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.security.xsrf.RequiresXsrfCheck;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.web.action.JiraWebActionSupport;

import ru.andreymarkelov.atlas.plugins.todos.manager.ToDoData;
import ru.andreymarkelov.atlas.plugins.todos.model.ToDoDataItem;
import ru.andreymarkelov.atlas.plugins.todos.model.ToDoFieldData;

public class ToDoListConfigAction extends JiraWebActionSupport {
    private static final long serialVersionUID = 2582939781550062892L;

    private final ToDoData pluginData;
    private final CustomFieldManager cfMgr;

    private List<ToDoFieldData> fields;
    private String currentField;
    private ToDoFieldData currentFieldData;
    private String reporter;
    private String nobody;
    private String showPanel;

    public ToDoListConfigAction(ToDoData pluginData, CustomFieldManager cfMgr) {
        this.pluginData = pluginData;
        this.cfMgr = cfMgr;
        this.fields= new ArrayList<ToDoFieldData>();
    }

    public String doConfigure() throws Exception {
        if (!hasAdminPermission()) {
            return PERMISSION_VIOLATION_RESULT;
        }

        this.currentFieldData = null;
        if (this.currentField != null) {
            CustomField cf = cfMgr.getCustomFieldObject(this.currentField);
            if (cf != null) {
                ToDoDataItem data = pluginData.getToDoDataItem(cf.getId());
                if (data != null) {
                    this.currentFieldData = new ToDoFieldData(cf.getId(), cf.getName(), data);
                    this.reporter = Boolean.toString(data.isReporter());
                    this.nobody = Boolean.toString(data.isNobody());
                    this.showPanel = Boolean.toString(data.isShowPanel());
                } else {
                    this.currentFieldData = new ToDoFieldData(cf.getId(), cf.getName());
                    this.reporter = Boolean.FALSE.toString();
                    this.nobody = Boolean.FALSE.toString();
                    this.showPanel = Boolean.FALSE.toString();
                }
            }
        } else {
            return getRedirect("ToDoListConfigAction!default.jspa");
        }
        return "configure";
    }

    @Override
    public String doDefault() throws Exception {
        if (!hasAdminPermission()) {
            return PERMISSION_VIOLATION_RESULT;
        }

        this.fields= new ArrayList<ToDoFieldData>();
        List<CustomField> cgList = cfMgr.getCustomFieldObjects();
        for (CustomField cf : cgList) {
            if (cf.getCustomFieldType().getKey().equals("ru.mail.jira.plugins.todolist:todo-list-custom-field")) {
                ToDoDataItem data = pluginData.getToDoDataItem(cf.getId());
                if (data != null) {
                    this.fields.add(new ToDoFieldData(cf.getId(), cf.getName(), data));
                } else {
                    this.fields.add(new ToDoFieldData(cf.getId(), cf.getName()));
                }
            }
        }
        return SUCCESS;
    }

    @Override
    @RequiresXsrfCheck
    protected String doExecute() throws Exception {
        pluginData.setToDoDataItem(
            currentField,
            new ToDoDataItem(
                Boolean.parseBoolean(reporter),
                Boolean.parseBoolean(nobody),
                Boolean.parseBoolean(showPanel),
                "",
                null,
                null));
        if (isInlineDialogMode()) {
            return returnComplete();
        }
        return getRedirect("ToDoListConfigAction!default.jspa");
    }

    public String getCurrentField() {
        return currentField;
    }

    public ToDoFieldData getCurrentFieldData() {
        return currentFieldData;
    }

    public List<ToDoFieldData> getFields() {
        return fields;
    }

    public String getNobody() {
        return nobody;
    }

    public String getReporter() {
        return reporter;
    }

    public String getShowPanel() {
        return showPanel;
    }

    public boolean hasAdminPermission() {
        ApplicationUser user = getLoggedInUser();
        if (user == null) {
            return false;
        }
        return getGlobalPermissionManager().hasPermission(GlobalPermissionKey.ADMINISTER, getLoggedInUser());
    }

    public void setCurrentField(String currentField) {
        this.currentField = currentField;
    }

    public void setNobody(String nobody) {
        this.nobody = nobody;
    }

    public void setReporter(String reporter) {
        this.reporter = reporter;
    }

    public void setShowPanel(String showPanel) {
        this.showPanel = showPanel;
    }
}
