package ru.andreymarkelov.atlas.plugins.todos.field;

import ru.andreymarkelov.atlas.plugins.todos.model.ToDoItem;
import ru.andreymarkelov.atlas.plugins.todos.util.ToDoUtils;

import java.util.Set;

public class ToDoListCfHelperImpl implements ToDoListCfHelper {
    @Override
    public Set<ToDoItem> getItems(String value) {
        return ToDoUtils.getParsedValue(value);
    }
}
