package ru.andreymarkelov.atlas.plugins.todos.model;

public class HistoryToDoFieldData {
    private String fieldName;
    private String fieldId;
    private String oldValue;
    private String value;
    private Long historyId;
    private String executor;
    private long time;

    public String getExecutor() {
        return executor;
    }

    public String getFieldId() {
        return fieldId;
    }

    public String getFieldName() {
        return fieldName;
    }

    public Long getHistoryId() {
        return historyId;
    }

    public String getOldValue() {
        return oldValue;
    }

    public long getTime() {
        return time;
    }

    public String getValue() {
        return value;
    }

    public void setExecutor(String executor) {
        this.executor = executor;
    }

    public void setFieldId(String fieldId) {
        this.fieldId = fieldId;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public void setHistoryId(Long historyId) {
        this.historyId = historyId;
    }

    public void setOldValue(String oldValue) {
        this.oldValue = oldValue;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "HistoryToDoFieldData[fieldName=" + fieldName + ", fieldId="
            + fieldId + ", oldValue=" + oldValue + ", value=" + value
            + ", historyId=" + historyId + ", executor=" + executor
            + ", time=" + time + "]";
    }
}
