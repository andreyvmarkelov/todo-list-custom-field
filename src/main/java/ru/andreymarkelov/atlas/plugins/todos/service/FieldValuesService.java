package ru.andreymarkelov.atlas.plugins.todos.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

@Path("todolistvalues")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
@AnonymousAllowed
public class FieldValuesService {
    public FieldValuesService() {
    }
}
