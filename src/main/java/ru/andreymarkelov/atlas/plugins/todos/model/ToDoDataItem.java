package ru.andreymarkelov.atlas.plugins.todos.model;

import java.util.ArrayList;
import java.util.List;

public class ToDoDataItem {
    private boolean isReporter;
    private boolean isNobody;
    private boolean showPanel;
    private String style;
    private List<String> groups;
    private List<String> statuses;

    public ToDoDataItem() {
        this.groups = new ArrayList<String>();
        this.statuses = new ArrayList<String>();
    }

    public ToDoDataItem(
            boolean isReporter,
            boolean isNobody,
            boolean showPanel,
            String style,
            List<String> groups,
            List<String> statuses) {
        this.isReporter = isReporter;
        this.isNobody = isNobody;
        this.showPanel = showPanel;
        this.style = style;
        this.groups = groups;
        this.statuses = statuses;
    }

    public List<String> getGroups() {
        return groups;
    }

    public List<String> getStatuses() {
        return statuses;
    }

    public String getStyle() {
        return style;
    }

    public boolean isNobody() {
        return isNobody;
    }

    public boolean isReporter() {
        return isReporter;
    }

    public boolean isShowPanel() {
        return showPanel;
    }

    public void setGroups(List<String> groups) {
        this.groups = groups;
    }

    public void setNobody(boolean isNobody) {
        this.isNobody = isNobody;
    }

    public void setReporter(boolean isReporter) {
        this.isReporter = isReporter;
    }

    public void setShowPanel(boolean showPanel) {
        this.showPanel = showPanel;
    }

    public void setStatuses(List<String> statuses) {
        this.statuses = statuses;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    @Override
    public String toString() {
        return "ToDoDataItem[isReporter=" + isReporter + ", isNobody="
            + isNobody + ", showPanel=" + showPanel + ", style=" + style
            + ", groups=" + groups + ", statuses=" + statuses + "]";
    }
}
