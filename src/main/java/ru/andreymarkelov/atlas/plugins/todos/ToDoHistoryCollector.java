package ru.andreymarkelov.atlas.plugins.todos;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.ofbiz.core.entity.GenericValue;

import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.changehistory.ChangeHistory;
import com.atlassian.jira.issue.changehistory.ChangeHistoryManager;
import com.atlassian.jira.issue.fields.CustomField;

import ru.andreymarkelov.atlas.plugins.todos.field.ToDoListCf;
import ru.andreymarkelov.atlas.plugins.todos.manager.ToDoData;
import ru.andreymarkelov.atlas.plugins.todos.model.HistoryToDoFieldData;
import ru.andreymarkelov.atlas.plugins.todos.model.ToDoDataItem;

public class ToDoHistoryCollector {
    private final ChangeHistoryManager changeHistoryManager;
    private final CustomFieldManager customFieldManager;
    private final ToDoData pluginData;

    public ToDoHistoryCollector(
            ToDoData pluginData,
            ChangeHistoryManager changeHistoryManager,
            CustomFieldManager customFieldManager) {
        this.pluginData = pluginData;
        this.changeHistoryManager = changeHistoryManager;
        this.customFieldManager = customFieldManager;
    }

    public List<HistoryToDoFieldData> getIssueTodos(Issue issue) {
        List<HistoryToDoFieldData> res = new ArrayList<HistoryToDoFieldData>();
        Map<String, String> todoFields = getToDoFieldNames();
        List<ChangeHistory> histories = changeHistoryManager.getChangeHistories(issue);
        for (ChangeHistory history : histories) {
            List<?> items = history.getChangeItems();
            for (Object item : items) {
                GenericValue cib = (GenericValue) item;
                String field = cib.getString("field");
                String fieldtype = cib.getString("fieldtype");
                if (fieldtype.equals("custom") && todoFields.containsKey(field)) {
                    ToDoDataItem data = pluginData.getToDoDataItem(todoFields.get(field));
                    boolean includeField = (data != null && data.isShowPanel());
                    if (includeField) {
                        HistoryToDoFieldData htfd = new HistoryToDoFieldData();
                        htfd.setFieldName(field);
                        htfd.setFieldId(todoFields.get(field));
                        htfd.setValue(cib.getString("newstring"));
                        htfd.setOldValue(cib.getString("oldstring"));
                        htfd.setExecutor(history.getAuthorObject().getName());
                        htfd.setTime(history.getTimePerformed().getTime());
                        htfd.setHistoryId(history.getId());
                        res.add(htfd);
                    }
                }
            }
        }
        return res;
    }

    public Map<String, String> getToDoFieldNames() {
        Map<String, String> res = new HashMap<String, String>();
        List<CustomField> cfs = customFieldManager.getCustomFieldObjects();
        for (CustomField cf : cfs) {
            if (cf.getCustomFieldType().getClass().equals(ToDoListCf.class)) {
                res.put(cf.getName(), cf.getId());
            }
        }
        return res;
    }

    public Map<String, String> getToDoFieldNames(Issue issue) {
        Map<String, String> res = new HashMap<String, String>();
        List<CustomField> cfs = customFieldManager.getCustomFieldObjects(issue);
        for (CustomField cf : cfs) {
            if (cf.getCustomFieldType().getClass().equals(ToDoListCf.class)) {
                res.put(cf.getName(), cf.getId());
            }
        }
        return res;
    }
}
